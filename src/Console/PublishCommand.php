<?php

namespace NeoScrypts\Server\Console;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:publish {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the Server Docker files';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', ['--tag' => 'neoscrypts-server', '--force' => $this->option('force')]);

        file_put_contents(
            $this->laravel->basePath('docker-compose.yml'),
            str_replace('./vendor/neoscrypts/server/docker', './docker', file_get_contents($this->laravel->basePath('docker-compose.yml')))
        );
    }
}
