<?php

namespace NeoScrypts\Server\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:install {--with= : The services that should be included in the installation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Server\'s default Docker Compose file';

    /**
     * Supported services to install
     *
     * @var string[]
     */
    protected array $supported = [
        'mysql',
        'pgsql',
        'mariadb',
        'redis',
        'memcached',
        'meilisearch',
        'mailhog',
    ];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->option('no-interaction')) {
            $services = ['mysql', 'redis', 'pgsql'];
        } else if ($this->option('with')) {
            $services = explode(',', $this->option('with'));
        } else {
            $services = $this->chooseServices();
        }

        $services = collect($services)
            ->map(function ($name){
                return trim($name);
            })
            ->filter(function ($name) {
                return in_array($name, $this->supported);
            })
            ->toArray();

        $this->buildDockerCompose($services);
        $this->updateEnvVariables($services);
        $this->info('Server scaffolding installed.');
    }

    /**
     * Gather the desired Server services using a Symfony menu.
     *
     * @return string|array
     */
    protected function chooseServices()
    {
        return $this->choice('Which services would you like to install?', $this->supported, 0, null, true);
    }

    /**
     * Build the Docker Compose file.
     *
     * @param array $services
     * @return void
     */
    protected function buildDockerCompose(array $services)
    {
        $depends = collect($services)
            ->filter(function ($name) {
                return in_array($name, ['mysql', 'pgsql', 'mariadb', 'redis']);
            })
            ->map(function ($name) {
                return "            - $name";
            })
            ->implode("\n");

        $stub = collect($services)
            ->map(function ($name) {
                return file_get_contents(__DIR__ . "/../../stubs/$name.stub");
            })
            ->implode('');

        $volumes = collect($services)
            ->filter(function ($name) {
                return in_array($name, ['mysql', 'pgsql', 'mariadb', 'redis', 'meilisearch']);
            })
            ->map(function ($name) {
                return "    neoscrypts-$name:\n        driver: local";
            })
            ->implode("\n");

        $dockerCompose = file_get_contents(__DIR__ . '/../../stubs/docker-compose.stub');

        $dockerCompose = str_replace('{{depends}}', $depends, $dockerCompose);
        $dockerCompose = str_replace('{{services}}', rtrim($stub), $dockerCompose);
        $dockerCompose = str_replace('{{volumes}}', $volumes, $dockerCompose);

        // Remove empty lines...
        $dockerCompose = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $dockerCompose);

        file_put_contents($this->laravel->basePath('docker-compose.yml'), $dockerCompose);
    }

    /**
     * Replace the Host environment variables in the app's .env file.
     *
     * @param array $services
     * @return void
     */
    protected function updateEnvVariables(array $services)
    {
        $env = file_get_contents($this->laravel->basePath('.env'));

        if (in_array('memcached', $services)) {
            $env = preg_replace("/MEMCACHED_HOST=(.*)/", 'MEMCACHED_HOST=memcached', $env);
        }

        if (in_array('redis', $services)) {
            $env = preg_replace("/REDIS_HOST=(.*)/", 'REDIS_HOST=redis', $env);
        }

        if (in_array('meilisearch', $services)) {
            $env .= "\nSCOUT_DRIVER=meilisearch\nMEILISEARCH_HOST=http://meilisearch:7700\n";
        }

        file_put_contents($this->laravel->basePath('.env'), $env);
    }
}
